nginx-variables:
  nginx_ssl_enable: true
  nginx_websockets_enable: true
  nginx_forge_enable: true
  nginx_phoenix_enable: true
  sticky: off
  use_uri_prefix: false


