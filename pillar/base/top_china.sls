base:
  'role:mongo-config':
    - match: grain
    - mongodb
  'role:mongo-shard':
    - match: grain
    - mongodb
  'role:node-server':
    - match: grain
    - mongodb
  'role:mongo-dev':
    - match: grain
    - mongodb
  'role:node-testing':
    - match: grain
    - mongodb
  'role:push-server':
    - match: grain
    - mongodb
