#!py

import logging
import subprocess
from  slacker import Slacker

slack = Slacker('xoxp-6650113031-23591072132-85527501782-ac1b4874736dcbbd90709dfcfed24ff6')

log = logging.getLogger(__name__)

def run():
    user = data['post']['user_name']
    if data['post']['text']:
        cmd = "salt idr-mongo-dev2 mongosync.exportConfig " + "collections=" + data['post']['text'] + " " + "user=" + data['post']['user_name']
    else:
        cmd = "salt idr-mongo-dev2 test.ping" 
        log.debug(data['post']['text'])

    subprocess.call(cmd, shell=True)
    log.info(data['post']['text'])
    return {}
