#!py

import boto3
import git
import subprocess
import logging
import time
from  slacker import Slacker


log = logging.getLogger(__name__)
slack = Slacker('xoxb-85824758384-WtQhsyrfelfI1mfytbBrk6EB')

master = ['us-west-2','ap-northeast-2','ap-southeast-1','us-east-2']
qa = ['ap-northeast-2']
alpha = ['sa-east-1']
#live = ['ca-central-1']
live = ['us-east-2','us-east-1','us-west-1','us-west-2','ap-south-1','ap-northeast-2','ap-southeast-1','ap-southeast-2','ap-northeast-1','ca-central-1','eu-central-1','eu-west-1','eu-west-2','sa-east-1']

commonParameters = '-batchmode -nographics -startPort 26000 -endPort 26999 -awsKeyId "AKIAJXKNSSK2GYPDJAMQ" -awsKeySecret "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"'

qaParameters= {
    'apisUrl' : 'https://api-qa.next.rndmi.com',
    'scApisUrl': 'https://sc-api-qa.next.rndmi.com/',
    'appKey': '21943d8f-5aba-11e7-bd95-066f264b7316',
    'logsGroup':'sc-gamelift-qa'
}
alphaParameters= {
    'apisUrl' : 'https://api-alpha.next.rndmi.com',
    'scApisUrl': 'https://sc-api-alpha.next.rndmi.com/',
    'appKey': 'a61deca3-07a8-11e8-990a-067837dd3ff6',
    'logsGroup':'sc-gamelift-alpha'
}
liveParameters= {
    'apisUrl' : 'https://api.live.rndmi.com',
    'scApisUrl': 'https://sc-api.live.rndmi.com/',
    'appKey': '2d32a187-d391-11e6-a601-1235f9e470a4',
    'logsGroup':'sc-gamelift-live'
}
devParameters= {
    'apisUrl' : 'https://api-dev.next.rndmi.com',
    'scApisUrl': 'https://sc-api-dev.next.rndmi.com/',
    'appKey': 'f9880469-9fc2-11e6-8fa0-06a99411b809',
    'logsGroup':'sc-gamelift-dev'
}

def upload_build(branch,commit):
    build_ids = {}
    if branch == 'qa': awsregions = qa
    if branch == 'alpha': awsregions = alpha
    if branch == 'live': awsregions = live
    if branch == 'master': awsregions = master
    for awsregion in awsregions:
        upload_build_command = 'aws gamelift upload-build --region {build_region} --name {build_name} --build-version {build_version} --operating-system AMAZON_LINUX  --build-root {build_root}'.format(
           build_version = commit,
           build_region = awsregion,
           build_name = 'pvp-server-' + branch,
           build_root = '/root/rb-server-docker')

        log.debug("Uploading build by executing command: ")
        log.debug(upload_build_command)
    
        build_output = subprocess.check_output(upload_build_command, shell=True, universal_newlines=True)

        # Grab the newly generated build id from the second last line of the command output
        build_ids[awsregion] =  build_output.split("\n")[-2].split()[2]

    return build_ids

def create_fleet(build_ids,branch,commithash):
    if branch == 'qa': 
        param = qaParameters
        fleetType = ['SPOT']
    if branch == 'alpha': 
        param = alphaParameters
        fleetType = ['SPOT']
    if branch == 'live': 
        param = liveParameters
        fleetType = ['SPOT','ON_DEMAND']
    if branch == 'master': 
        param = devParameters
        fleetType = ['SPOT']
    for key,val in build_ids.items():
        for Ftype in fleetType: 
            client = boto3.client('gamelift', region_name= key)
            response = client.create_fleet(
                Name= branch + '-' + Ftype + '-' +  commithash,
                Description= branch + "-" + Ftype,
                BuildId= val,
                EC2InstanceType='c4.large',
                EC2InboundPermissions=[
                    {
                        'FromPort': 26000,
                        'ToPort': 26999,
                        'IpRange': '0.0.0.0/0',
                        'Protocol': 'TCP'
                    },
                    {
                        'FromPort': 26000,
                        'ToPort': 26999,
                        'IpRange': '0.0.0.0/0',
                        'Protocol': 'UDP'
                    },
                ],
                NewGameSessionProtectionPolicy='FullProtection',
                RuntimeConfiguration={
                    'ServerProcesses': [
                        {
                            'LaunchPath': '/local/game/server/rb-server.x86_64',
                            'Parameters': '{common}  -apisUrl {apisUrl} -scApisUrl {scApisUrl} -appKey {appKey} -logsRegion {logRegion} -logsGroup {logsGroup}  -hostName {hostName}'.format(
                                common = commonParameters,
                                apisUrl = param['apisUrl'],
                                scApisUrl = param['scApisUrl'],
                                appKey = param['appKey'],
                                logRegion = 'us-west-2',
                                logsGroup = param['logsGroup'],
                                hostName = 'pvp-' + branch + '-' + key + '.gamelift'
                            ),
                            'ConcurrentExecutions': 12
                        },
                    ],
                    'MaxConcurrentGameSessionActivations': 1,
                    'GameSessionActivationTimeoutSeconds': 60
                },
                MetricGroups=[
                    branch + '-' + Ftype + '-' + key + '-1',
                ],
                FleetType= Ftype
            )
            fleetId = response['FleetAttributes']['FleetId']
            client.update_fleet_capacity(
                FleetId= fleetId,
                DesiredInstances=1,
                MinSize=0,
                MaxSize=1
            )
            client.put_scaling_policy(
               Name= 'TargetBased',
               FleetId= fleetId,
               MetricName='PercentAvailableGameSessions',
               PolicyType='TargetBased',
               TargetConfiguration={
                   'TargetValue': 15
               }
            )
#            client.put_scaling_policy(
#               Name= 'BURST2',
#               FleetId= fleetId,
#               ScalingAdjustment=2,
#               ScalingAdjustmentType='ChangeInCapacity',
#               Threshold=48,
#               ComparisonOperator='GreaterThanOrEqualToThreshold',
#               EvaluationPeriods=1,
#               MetricName='ActivatingGameSessions',
#               PolicyType='RuleBased'
#            )
#            client.put_scaling_policy(
#               Name= 'BURST1',
#               FleetId= fleetId,
#               ScalingAdjustment=1,
#               ScalingAdjustmentType='ChangeInCapacity',
#               Threshold=24,
#               ComparisonOperator='GreaterThanOrEqualToThreshold',
#               EvaluationPeriods=1,
#               MetricName='ActivatingGameSessions',
#               PolicyType='RuleBased'
#            )
#            client.put_scaling_policy(
#               Name= 'DOWN',
#               FleetId= fleetId,
#               ScalingAdjustment= -1,
#               ScalingAdjustmentType='ChangeInCapacity',
#               Threshold=24,
#               ComparisonOperator='GreaterThanOrEqualToThreshold',
#               EvaluationPeriods=5,
#               MetricName='AvailableGameSessions',
#               PolicyType='RuleBased'
#            )
#            client.put_scaling_policy(
#               Name= 'SLEEP',
#               FleetId= fleetId,
#               ScalingAdjustment= 1,
#               ScalingAdjustmentType='ExactCapacity',
#               Threshold=5,
#               ComparisonOperator='LessThanOrEqualToThreshold',
#               EvaluationPeriods=10,
#               MetricName='ActiveGameSessions',
#               PolicyType='RuleBased'
#            )
#            client.put_scaling_policy(
#               Name= 'QUEUE',
#               FleetId= fleetId,
#               ScalingAdjustment=1,
#               ScalingAdjustmentType='ChangeInCapacity',
#               Threshold=10,
#               ComparisonOperator='GreaterThanOrEqualToThreshold',
#               EvaluationPeriods=1,
#               MetricName='QueueDepth',
#               PolicyType='RuleBased'
#            )
#            client.put_scaling_policy(
#               Name= 'UP',
#               FleetId= fleetId,
#               ScalingAdjustment= 1,
#               ScalingAdjustmentType='ChangeInCapacity',
#               Threshold=8,
#               ComparisonOperator='LessThanOrEqualToThreshold',
#               EvaluationPeriods=1,
#               MetricName='AvailableGameSessions',
#               PolicyType='RuleBased'
#            )
            log.debug('the fleet id seems to be this =>' + fleetId)
    return 

def run():
    auth_users = ['sbalouria','plamen']
    git_dir = '/root/rb-server-docker'
    branch = data['post']['text']
    mssg = 'Not an authorized user to create fleet/build:-\n ' \
            "User: " + data['post']['user_name']
    if data['post']['user_name'] in auth_users:
        g = git.cmd.Git(git_dir)
        log.debug("This is the branch:  " + branch)
        g.checkout(branch)
        g.pull()
        return_string = g.log('-1','--pretty=oneline')
        commithash = return_string.split(' ', 1)[0]    
        build_ids = upload_build(branch,commithash)
        slack.chat.post_message('#gamelift-builds',"Build triggered by" + ":" + data['post']['user_name']  + "\n" + "branch: " +  branch + "\n"  + "commit: " +  return_string + "\n",'saltstackbot' )
        
        for key,val in build_ids.items():
            slack.chat.post_message('#gamelift-builds',"Region/BuildID" + "==> " + key +" : " +  str(val),'saltstackbot' )
        time.sleep(60)
        if branch == 'qa' or branch == 'master' or branch == 'alpha' or branch == 'live':
            create_fleet(build_ids,branch,commithash)
            
    else:
        slack.chat.post_message('#gamelift-builds',mssg,'saltstackbot' )
    return {}

