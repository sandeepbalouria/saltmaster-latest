#!py

import git
import subprocess
import logging
from  slacker import Slacker

log = logging.getLogger(__name__)
slack = Slacker('xoxb-85824758384-WtQhsyrfelfI1mfytbBrk6EB')

master = ['us-west-2','ap-northeast-2','ap-southeast-1','us-east-2']
qa = ['ap-northeast-2']
alpha = ['sa-east-1']
live = ['us-east-2','us-east-1','us-west-1','us-west-2','ap-south-1','ap-northeast-2','ap-southeast-1','ap-southeast-2','ap-northeast-1','ca-central-1','eu-central-1','eu-west-1','eu-west-2','sa-east-1']
#live = ['sa-east-1']

def upload_build(branch):
    build_ids = []
    if branch == 'qa': awsregions = qa
    if branch == 'alpha': awsregions = alpha
    if branch == 'live': awsregions = live
    if branch == 'master': awsregions = master
    for awsregion in awsregions:
        upload_build_command = 'aws gamelift upload-build --region {build_region} --name {build_name} --build-version {build_version} --operating-system AMAZON_LINUX  --build-root {build_root}'.format(
           build_version = data['post']['after'],
           build_region = awsregion,
           build_name = 'pvp-servver-' + branch,
           build_root = '/root/rb-server-docker')

        log.debug("Uploading build by executing command: ")
        log.debug(upload_build_command)
    
        build_output = subprocess.check_output(upload_build_command, shell=True, universal_newlines=True)

        # Grab the newly generated build id from the second last line of the command output
        build_ids.append( build_output.split("\n")[-2].split()[2])

    return build_ids

def run():
    git_dir = '/root/rb-server-docker'
    ref = data['post']['ref']
    g = git.cmd.Git(git_dir)
    a,b,branch = ref.split("/")
    log.debug("This is the branch:  " + branch)
    g.checkout(branch)
    g.pull()
    return_string = g.log('-1','--pretty=oneline')
    build_ids = upload_build(branch)
    for buildid in build_ids:
        slack.chat.post_message('#gamelift-builds',"Build triggered by" + ":" + data['post']['commits'][0]['author']['name'] + "\n" + "branch: " +  branch + "\n"  + "commit: " +  return_string + "\n"  + "gamelift build ID: " +  str(buildid),'saltstackbot' )
    log.debug("git log: " + return_string)
    return {}
