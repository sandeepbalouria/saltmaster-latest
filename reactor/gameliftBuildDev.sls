#!py

import boto3
import git
import subprocess
import logging
import time
from  slacker import Slacker


log = logging.getLogger(__name__)
slack = Slacker('xoxb-85824758384-WtQhsyrfelfI1mfytbBrk6EB')

master = ['us-west-2','ap-northeast-2','ap-southeast-1','us-east-2']
qa = ['ap-northeast-2']
alpha = ['sa-east-1']
live = ['us-east-2','us-east-1','us-west-1','us-west-2','ap-south-1','ap-northeast-2','ap-southeast-1','ap-southeast-2','ap-northeast-1','ca-central-1','eu-central-1','eu-west-1','eu-west-2','sa-east-1']

commonParameters = '-batchmode -nographics -startPort 26000 -endPort 26999 -awsKeyId "AKIAJXKNSSK2GYPDJAMQ" -awsKeySecret "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"'

qaParameters= {
    'apisUrl' : 'https://api-qa.next.rndmi.com',
    'scApisUrl': 'https://sc-api-qa.next.rndmi.com/',
    'appKey': '21943d8f-5aba-11e7-bd95-066f264b7316',
    'logsGroup':'sc-gamelift-qa'
}
alphaParameters= {
    'apisUrl' : 'https://api-alpha.next.rndmi.com',
    'scApisUrl': 'https://sc-api-alpha.next.rndmi.com/',
    'appKey': 'a61deca3-07a8-11e8-990a-067837dd3ff6',
    'logsGroup':'sc-gamelift-alpha'
}
liveParameters= {
    'apisUrl' : 'https://api.live.rndmi.com',
    'scApisUrl': 'https://sc-api.live.rndmi.com/',
    'appKey': '2d32a187-d391-11e6-a601-1235f9e470a4',
    'logsGroup':'sc-gamelift-live'
}
devParameters= {
    'apisUrl' : 'https://api-dev.next.rndmi.com',
    'scApisUrl': 'https://sc-api-dev.next.rndmi.com/',
    'appKey': 'f9880469-9fc2-11e6-8fa0-06a99411b809',
    'logsGroup':'sc-gamelift-dev'
}

def upload_build(branch):
    build_ids = {}
    if branch == 'qa': awsregions = qa
    if branch == 'alpha': awsregions = alpha
    if branch == 'live': awsregions = live
    if branch == 'master': awsregions = master
    for awsregion in awsregions:
        upload_build_command = 'aws gamelift upload-build --region {build_region} --name {build_name} --build-version {build_version} --operating-system AMAZON_LINUX  --build-root {build_root}'.format(
           build_version = data['post']['after'],
           build_region = awsregion,
           build_name = 'pvp-servver-' + branch,
           build_root = '/root/rb-server-docker')

        log.debug("Uploading build by executing command: ")
        log.debug(upload_build_command)
    
        build_output = subprocess.check_output(upload_build_command, shell=True, universal_newlines=True)

        # Grab the newly generated build id from the second last line of the command output
        build_ids[awsregion] =  build_output.split("\n")[-2].split()[2]

    return build_ids

def create_fleet(build_ids,branch):
    if branch == 'qa': param = qaParameters
    if branch == 'alpha': param = alphaParameters
    if branch == 'live': param = liveParameters
    if branch == 'master': param = devParameters
    for key,val in build_ids.items():
        client = boto3.client('gamelift', region_name= key)
        response = client.create_fleet(
            Name='reactor-test',
            Description='testing',
            BuildId= val,
            EC2InstanceType='m4.large',
            EC2InboundPermissions=[
                {
                    'FromPort': 26000,
                    'ToPort': 26999,
                    'IpRange': '0.0.0.0/0',
                    'Protocol': 'TCP'
                },
                {
                    'FromPort': 26000,
                    'ToPort': 26999,
                    'IpRange': '0.0.0.0/0',
                    'Protocol': 'UDP'
                },
            ],
            NewGameSessionProtectionPolicy='FullProtection',
            RuntimeConfiguration={
                'ServerProcesses': [
                    {
                        'LaunchPath': '/local/game/server/rb-server.x86_64',
                        'Parameters': '{common}  -apisUrl {apisUrl} -scApisUrl {scApisUrl} -appKey {appKey} -logsRegion {logRegion} -logsGroup {logsGroup}  -hostName {hostName}'.format(
                            common = commonParameters,
                            apisUrl = param['apisUrl'],
                            scApisUrl = param['scApisUrl'],
                            appKey = param['appKey'],
                            logRegion = 'us-west-2',
                            logsGroup = param['logsGroup'],
                            hostName = 'pvp-' + branch + '-' + key + '.gamelift'
                        ),
                        'ConcurrentExecutions': 20
                    },
                ],
                'MaxConcurrentGameSessionActivations': 1,
                'GameSessionActivationTimeoutSeconds': 60
            },
            MetricGroups=[
                branch + '-' + 'spt-' + key + '-1',
            ],
            FleetType='SPOT'
        )
    log.debug(response)
    return response

def run():
    git_dir = '/root/rb-server-docker'
    ref = data['post']['ref']
    g = git.cmd.Git(git_dir)
    a,b,branch = ref.split("/")
    log.debug("This is the branch:  " + branch)
    g.checkout(branch)
    g.pull()
    return_string = g.log('-1','--pretty=oneline')

    build_ids = upload_build(branch)
    slack.chat.post_message('#gamelift-builds',"Build triggered by" + ":" + data['post']['commits'][0]['author']['name'] + "\n" + "branch: " +  branch + "\n"  + "commit: " +  return_string + "\n",'saltstackbot' )
    
    for key,val in build_ids.items():
        slack.chat.post_message('#gamelift-builds',"Region/BuildID" + "==> " + key +" : " +  str(val),'saltstackbot' )
    time.sleep(60)
    if branch == 'qa':
        resp = create_fleet(build_ids,branch)
        log.debug(resp)
    return {}

