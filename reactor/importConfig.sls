#!py

import logging
import subprocess
from  slacker import Slacker

slack = Slacker('xoxb-85824758384-WtQhsyrfelfI1mfytbBrk6EB')

log = logging.getLogger(__name__)

def run():
    mssg = 'Staging synced with dev:-\n ' \
            "Collections: " + data['post']['collections'] + "\n" \
            "User: " + data['post']['user']
    cmd = "salt idr-test-pushserver  mongosync.importConfig " + data['post']['key'] + " " + data['post']['collections']
    subprocess.call(cmd, shell=True)
    slack.chat.post_message('#idrstaging',mssg,'saltstackbot' )
    return {}
