#!py

import requests
import logging
from  slacker import Slacker

log = logging.getLogger(__name__)
slack = Slacker('xoxb-85824758384-WtQhsyrfelfI1mfytbBrk6EB')
auth_users = ['sbalouria','plamen']

webhooks = {
    "ri_supercharged_api": "https://openshift-next.rndmi.com:8443/oapi/v1/namespaces/supercharged/buildconfigs/supercharged-api/webhooks/%40p1_%244cr47/generic",
    "accounts_grps": "https://openshift-next.rndmi.com:8443/oapi/v1/namespaces/backend/buildconfigs/accounts-grpc/webhooks/GRPC-%243cr37/generic",
    "admin-service": "https://openshift-next.rndmi.com:8443/oapi/v1/namespaces/backend/buildconfigs/admin-grpc/webhooks/GRPC-%243cr37/generic",
    "ri_api": "https://openshift-next.rndmi.com:8443/oapi/v1/namespaces/backend/buildconfigs/api-gateway/webhooks/%40p1_%244cr47/generic",
    "appdata-service": "https://openshift-next.rndmi.com:8443/oapi/v1/namespaces/backend/buildconfigs/appdata-grpc/webhooks/GRPC-%243cr37/generic",
    "user-service": "https://openshift-next.rndmi.com:8443/oapi/v1/namespaces/backend/buildconfigs/auth-grpc/webhooks/GRPC-%243cr37/generic",
    "backenddb": "https://openshift-next.rndmi.com:8443/oapi/v1/namespaces/backend/buildconfigs/db-migrations/webhooks/D%26_%244cr47/generic",
    "inventory-service": "https://openshift-next.rndmi.com:8443/oapi/v1/namespaces/backend/buildconfigs/inventory-grpc/webhooks/GRPC-%243cr37/generic",
    "leaderboards-service": "https://openshift-next.rndmi.com:8443/oapi/v1/namespaces/backend/buildconfigs/leaderboards-grpc/webhooks/GRPC-%243cr37/generic",
    "match-service": "https://openshift-next.rndmi.com:8443/oapi/v1/namespaces/backend/buildconfigs/matches-grpc/webhooks/GRPC-%243cr37/generic",
    "nats": "https://openshift-next.rndmi.com:8443/oapi/v1/namespaces/backend/buildconfigs/nats/webhooks/N%407%24-%243cr37/generic",
    "whip": "https://openshift-next.rndmi.com:8443/oapi/v1/namespaces/backend/buildconfigs/whip/webhooks/wh1p_%244cr47/generic",
    "push-server": "https://openshift-next.rndmi.com:8443/oapi/v1/namespaces/backend/buildconfigs/push-server/webhooks/7u%24%23_%244cr47/generic"
}
s = requests.Session()

def run():
    mssg = 'Not an authorized user to update pvpserver:-\n ' \
            "User: " + data['post']['user_name']
    if data['post']['user_name'] in auth_users:
        for k, v in webhooks.items():
            r = s.post(v, verify=False)
            slack.chat.post_message('#osnextbuilds',k + ":" + str(r.status_code),'saltstackbot' )
    else:
        slack.chat.post_message('#osnextbuilds',mssg,'saltstackbot' )

    return {}
