#!py

import requests
import logging
from  slacker import Slacker

log = logging.getLogger(__name__)
slack = Slacker('xoxb-85824758384-WtQhsyrfelfI1mfytbBrk6EB')
s = requests.Session()

dev_webhooks = {
    "ri_supercharged_api": "https://openshift-dev.rndmi.com:8443/oapi/v1/namespaces/supercharged-dev/buildconfigs/supercharged-api/webhooks/%40p1_%244cr47/generic",
    "accounts_grpc": "https://openshift-dev.rndmi.com:8443/oapi/v1/namespaces/backend-dev/buildconfigs/accounts-grpc/webhooks/GRPC-%243cr37/generic",
    "admin-service": "https://openshift-dev.rndmi.com:8443/oapi/v1/namespaces/backend-dev/buildconfigs/admin-grpc/webhooks/GRPC-%243cr37/generic",
    "ri_api": "https://openshift-dev.rndmi.com:8443/oapi/v1/namespaces/backend-dev/buildconfigs/api-gateway/webhooks/%40p1_%244cr47/generic",
    "appdata-service": "https://openshift-dev.rndmi.com:8443/oapi/v1/namespaces/backend-dev/buildconfigs/appdata-grpc/webhooks/GRPC-%243cr37/generic",
    "auth-grpc": "https://openshift-dev.rndmi.com:8443/oapi/v1/namespaces/backend-dev/buildconfigs/auth-grpc/webhooks/GRPC-%243cr37/generic",
    "backenddb": "https://openshift-dev.rndmi.com:8443/oapi/v1/namespaces/backend-dev/buildconfigs/db-migrations/webhooks/D%26_%244cr47/generic",
    "inventory-service": "https://openshift-dev.rndmi.com:8443/oapi/v1/namespaces/backend-dev/buildconfigs/inventory-grpc/webhooks/GRPC-%243cr37/generic",
    "leaderboards-service": "https://openshift-dev.rndmi.com:8443/oapi/v1/namespaces/backend-dev/buildconfigs/leaderboards-grpc/webhooks/GRPC-%243cr37/generic",
    "match-service": "https://openshift-dev.rndmi.com:8443/oapi/v1/namespaces/backend-dev/buildconfigs/matches-grpc/webhooks/GRPC-%243cr37/generic",
    "nats": "https://openshift-dev.rndmi.com:8443/oapi/v1/namespaces/backend-dev/buildconfigs/nats/webhooks/N%407%24-%243cr37/generic",
    "whip": "https://openshift-dev.rndmi.com:8443/oapi/v1/namespaces/backend-dev/buildconfigs/whip/webhooks/wh1p_%244cr47/generic",
    "push-server": "https://openshift-dev.rndmi.com:8443/oapi/v1/namespaces/backend-dev/buildconfigs/push-server/webhooks/7u%24%23_%244cr47/generic",
    "ri_admin_ui": "https://openshift-dev.rndmi.com:8443/oapi/v1/namespaces/backend-dev/buildconfigs/admin-ui/webhooks/%40dm1%23_%244cr47/generic",
    "location-service": "https://openshift-dev.rndmi.com:8443/oapi/v1/namespaces/backend-dev/buildconfigs/location-grpc/webhooks/GRPC-%243cr37/generic"
}

qa_webhooks = {
    "ri_supercharged_api": "https://openshift-dev.rndmi.com:8443/oapi/v1/namespaces/supercharged-qa/buildconfigs/supercharged-api/webhooks/%40p1_%244cr47/generic",
    "accounts_grpc": "https://openshift-dev.rndmi.com:8443/oapi/v1/namespaces/backend-qa/buildconfigs/accounts-grpc/webhooks/GRPC-%243cr37/generic",
    "admin-service": "https://openshift-dev.rndmi.com:8443/oapi/v1/namespaces/backend-qa/buildconfigs/admin-grpc/webhooks/GRPC-%243cr37/generic",
    "ri_api": "https://openshift-dev.rndmi.com:8443/oapi/v1/namespaces/backend-qa/buildconfigs/api-gateway/webhooks/%40p1_%244cr47/generic",
    "appdata-service": "https://openshift-dev.rndmi.com:8443/oapi/v1/namespaces/backend-qa/buildconfigs/appdata-grpc/webhooks/GRPC-%243cr37/generic",
    "auth-grpc": "https://openshift-dev.rndmi.com:8443/oapi/v1/namespaces/backend-qa/buildconfigs/auth-grpc/webhooks/GRPC-%243cr37/generic",
    "backenddb": "https://openshift-dev.rndmi.com:8443/oapi/v1/namespaces/backend-qa/buildconfigs/db-migrations/webhooks/D%26_%244cr47/generic",
    "inventory-service": "https://openshift-dev.rndmi.com:8443/oapi/v1/namespaces/backend-qa/buildconfigs/inventory-grpc/webhooks/GRPC-%243cr37/generic",
    "leaderboards-service": "https://openshift-dev.rndmi.com:8443/oapi/v1/namespaces/backend-qa/buildconfigs/leaderboards-grpc/webhooks/GRPC-%243cr37/generic",
    "match-service": "https://openshift-dev.rndmi.com:8443/oapi/v1/namespaces/backend-qa/buildconfigs/matches-grpc/webhooks/GRPC-%243cr37/generic",
    "nats": "https://openshift-dev.rndmi.com:8443/oapi/v1/namespaces/backend-qa/buildconfigs/nats/webhooks/N%407%24-%243cr37/generic",
    "whip": "https://openshift-dev.rndmi.com:8443/oapi/v1/namespaces/backend-qa/buildconfigs/whip/webhooks/wh1p_%244cr47/generic",
    "push-server": "https://openshift-dev.rndmi.com:8443/oapi/v1/namespaces/backend-qa/buildconfigs/push-server/webhooks/7u%24%23_%244cr47/generic",
    "ri_admin_ui": "https://openshift-dev.rndmi.com:8443/oapi/v1/namespaces/backend-qa/buildconfigs/admin-ui/webhooks/%40dm1%23_%244cr47/generic"
}
def run():
    branch = data['post']['push']['changes'][0]['new']['name'] 
    user = data['post']['actor']['username'] 
    repo = data['post']['repository']['name']
    if branch == "master":
        if repo == "user-services":
            r = s.post(dev_webhooks["auth-grpc"], verify=False)
            slack.chat.post_message('#osdevbuilds',"Build triggered by" + ":" + user + "\n" + "Repo: " + repo + "\n" + "Trigger status: " +  str(r.status_code),'saltstackbot' )
            r1 = s.post(dev_webhooks["accounts_grpc"], verify=False)
            slack.chat.post_message('#osdevbuilds',"Build triggered by" + ":" + user + "\n" + "Repo: " + repo + "\n" + "Trigger status: " +  str(r1.status_code),'saltstackbot' )
        elif repo == "ri_admin_ui":
            r = s.post(dev_webhooks[repo], verify=False)
            slack.chat.post_message('#osdevbuilds',"Build triggered by" + ":" + user + "\n" + "Repo: " + repo + "\n" + "Trigger status: " +  str(r.status_code),'saltstackbot' )
            r1 = s.post(qa_webhooks[repo], verify=False)
            slack.chat.post_message('#osqabuilds',"Build triggered by" + ":" + user + "\n" + "Repo: " + repo + "\n" + "Trigger status: " +  str(r1.status_code),'saltstackbot' )
        else:
            r = s.post(dev_webhooks[repo], verify=False)
            slack.chat.post_message('#osdevbuilds',"Build triggered by" + ":" + user + "\n" + "Repo: " + repo + "\n" + "Trigger status: " +  str(r.status_code),'saltstackbot' )
    elif branch == "qa":
        if repo == "user-services":
            r = s.post(qa_webhooks["auth-grpc"], verify=False)
            slack.chat.post_message('#osqabuilds',"Build triggered by" + ":" + user + "\n" + "Repo: " + repo + "\n" + "Trigger status: " +  str(r.status_code),'saltstackbot' )
            r1 = s.post(qa_webhooks["accounts_grpc"], verify=False)
            slack.chat.post_message('#osqabuilds',"Build triggered by" + ":" + user + "\n" + "Repo: " + repo + "\n" + "Trigger status: " +  str(r1.status_code),'saltstackbot' )
        else:
            r = s.post(qa_webhooks[repo], verify=False)
            slack.chat.post_message('#osqabuilds',"Build triggered by" + ":" + user + "\n" + "Repo: " + repo + "\n" + "Trigger status: " +  str(r.status_code),'saltstackbot' )
    else:
      log.debug(branch + ":" + user + ":" + repo) 
    return {}
