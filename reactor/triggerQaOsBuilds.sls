#!py

import requests
import logging
from  slacker import Slacker

log = logging.getLogger(__name__)
slack = Slacker('xoxb-85824758384-WtQhsyrfelfI1mfytbBrk6EB')
auth_users = ['sbalouria','plamen']
webhooks = {
    "ri_supercharged_api": "https://openshift-next.rndmi.com:8443/oapi/v1/namespaces/supercharged/buildconfigs/supercharged-api/webhooks/%40p1_%244cr47/generic",
    "accounts_grpc": "https://openshift-next.rndmi.com:8443/oapi/v1/namespaces/backend-qa/buildconfigs/accounts-grpc/webhooks/GRPC-%243cr37/generic",
    "admin-service": "https://openshift-next.rndmi.com:8443/oapi/v1/namespaces/backend-qa/buildconfigs/admin-grpc/webhooks/GRPC-%243cr37/generic",
    "ri_api": "https://openshift-next.rndmi.com:8443/oapi/v1/namespaces/backend-qa/buildconfigs/api-gateway/webhooks/%40p1_%244cr47/generic",
    "appdata-service": "https://openshift-next.rndmi.com:8443/oapi/v1/namespaces/backend-qa/buildconfigs/appdata-grpc/webhooks/GRPC-%243cr37/generic",
    "auth-grpc": "https://openshift-next.rndmi.com:8443/oapi/v1/namespaces/backend-qa/buildconfigs/auth-grpc/webhooks/GRPC-%243cr37/generic",
    "backenddb": "https://openshift-next.rndmi.com:8443/oapi/v1/namespaces/backend-qa/buildconfigs/db-migrations/webhooks/D%26_%244cr47/generic",
    "inventory-service": "https://openshift-next.rndmi.com:8443/oapi/v1/namespaces/backend-qa/buildconfigs/inventory-grpc/webhooks/GRPC-%243cr37/generic",
    "leaderboards-service": "https://openshift-next.rndmi.com:8443/oapi/v1/namespaces/backend-qa/buildconfigs/leaderboards-grpc/webhooks/GRPC-%243cr37/generic",
    "match-service": "https://openshift-next.rndmi.com:8443/oapi/v1/namespaces/backend-qa/buildconfigs/matches-grpc/webhooks/GRPC-%243cr37/generic",
    "nats": "https://openshift-next.rndmi.com:8443/oapi/v1/namespaces/backend-qa/buildconfigs/nats/webhooks/N%407%24-%243cr37/generic",
    "whip": "https://openshift-next.rndmi.com:8443/oapi/v1/namespaces/backend-qa/buildconfigs/whip/webhooks/wh1p_%244cr47/generic",
    "push-server": "https://openshift-next.rndmi.com:8443/oapi/v1/namespaces/backend-qa/buildconfigs/push-server/webhooks/7u%24%23_%244cr47/generic",
    "ri_admin_ui": "https://openshift-next.rndmi.com:8443/oapi/v1/namespaces/backend-qa/buildconfigs/admin-ui/webhooks/%40dm1%23_%244cr47/generic"
}

s = requests.Session()

def run():
    mssg = 'Not an authorized user to update pvpserver:-\n ' \
            "User: " + data['post']['user_name']
    if data['post']['user_name'] in auth_users:
        for k, v in webhooks.items():
            r = s.post(v, verify=False)
            slack.chat.post_message('#osqabuilds',k + ":" + str(r.status_code),'saltstackbot' )
    else:
        slack.chat.post_message('#osqabuilds',mssg,'saltstackbot' )

    return {}
