#!py

import logging
import subprocess
from  slacker import Slacker

slack = Slacker('xoxb-85824758384-WtQhsyrfelfI1mfytbBrk6EB')

log = logging.getLogger(__name__)

auth_users = ['sbalouria','plamen','sean','sam']

def run():
    mssg = 'Not an authorized user to update pvpserver:-\n ' \
            "User: " + data['post']['user_name']
    
    if data['post']['user_name'] in auth_users:
        cmd = "salt pvpserverhost updatepvpServer.update"
        mssg_out = subprocess.check_output(cmd, shell=True)
        slack.chat.post_message('#pvpserver',mssg_out,'saltstackbot' )
    else:
        slack.chat.post_message('#pvpserver',mssg,'saltstackbot' )
    
    return {}
