from boto.s3.connection import S3Connection
import time
import subprocess
import os, math
from filechunkio import FileChunkIO
import re
from boto.s3.key import Key
import logging 
import pymongo

log = logging.getLogger(__name__)

config_collections = ['buildingstats','chest_spawns','cards','cards_requirements','chests','buckets','unitstats','powerups','hardcoded_chests','daily_missions']


class RunCmd(object):
    def run_cmd(self, cmd):
        self.cmd = cmd
        subprocess.call(self.cmd, shell=True)

def _getMeHost():
    return __grains__['ip_interfaces']['eth0'][0]

def _filename():
    for f in os.listdir('/opt'):
        if re.match('configDump', f):
            return f

def _dumpCollections():
    """
    function takes a dump of the collections contaiing 
    config data
    """
    host = _getMeHost()
    stop_balancer = 'mongo --host ' + host + ' /opt/stopbalancer.js'
    start_balancer = 'mongo --host ' + host + ' /opt/startbalancer.js'
    tar_cmd = 'tar -cvf ' + '/opt/configDump'  + '.tar.gz'  + ' /opt/dump'
    #tar_cmd = 'tar -cvf ' + '/opt/configDump_' + re.sub('\s','_',time.asctime( time.localtime(time.time()) )) + '.tar.gz'  + ' /opt/dump'
    a = RunCmd()
    a.run_cmd("rm -rf /opt/configDump*")
    a.run_cmd("rm -rf /opt/opt")
    a.run_cmd("rm -rf /opt/dump")
    a.run_cmd(stop_balancer)
    for collection in config_collections:
        a.run_cmd('mongodump -d idrdev -c ' + collection + ' -h ' +  host + ' -o /opt/dump')
    a.run_cmd(start_balancer)
    a.run_cmd(tar_cmd)

def _upload():
    conn = S3Connection('AKIAJWNBZPUTEB5HGTMQ','UdmB+zvP4qG1AAWV4IDb0F8a8peNptPPDH3OCrWb')
    bucket = conn.get_bucket('devmongoconfigbackup')
    k = Key(bucket)
    k.Key = 'foobar'
    source_path = '/opt/' + _filename()
    k.set_contents_from_filename(source_path)

def _getMeRestoreCmd():
    host = _getMeHost()
    if __grains__['randenv'] == 'prod':
        cmd = 'mongorestore --host ' + host + ' -d idr /opt/opt/dump/idrdev'
    else:
        cmd = 'mongorestore  /opt/opt/dump'
    return cmd

def operateOnConfigColl(action):
    host = _getMeHost()
    client = pymongo.MongoClient(host, 27017)
    
    if __grains__['randenv'] == 'prod':
        db = client.idr
    else:
        db = client.idrdev

    if action == 'drop':
        for c in config_collections:
            log.info(c)
            db.drop_collection(c)
    elif action == 'create':
        for c in config_collections:
            db.create_collection(c)

def _import(key_value):
    host = _getMeHost()
    stop_balancer = 'mongo --host ' + host + ' /opt/stopbalancer.js'
    start_balancer = 'mongo --host ' + host + ' /opt/startbalancer.js'
    tar_cmd = 'tar -xvf ' + '/opt/'+ key_value + '.tar.gz'  + ' -C /opt'
    restore_cmd = _getMeRestoreCmd()
    a = RunCmd()
    a.run_cmd("rm -rf /opt/*.tar.gz")
    a.run_cmd("rm -rf /opt/opt")
    a.run_cmd(stop_balancer)
    conn = S3Connection('AKIAJWNBZPUTEB5HGTMQ','UdmB+zvP4qG1AAWV4IDb0F8a8peNptPPDH3OCrWb')
    bucket = conn.get_bucket('devmongoconfigbackup')
    key = bucket.get_key(key_value)
    key.get_contents_to_filename('/opt/' + key_value + '.tar.gz')
    a.run_cmd(tar_cmd)
    operateOnConfigColl('drop')
    a.run_cmd(restore_cmd)
    a.run_cmd(start_balancer)


def exportConfig():
    """
    """
    if __grains__['role'] == 'mongo-dev':
        _dumpCollections()
        _upload()
        return "config exported successfully"
        #_uploadToS3()
    else:
        return 'Not a mongo node'
def importConfig(key_value):
    """
    """
    _import(key_value)
    return "successfully imported"
    
if __name__ == '__main__':
    print 'hi'
