#! /usr/bin/python

from boto import ec2;
import boto.utils
import argparse
import time

tag = 'jenkins-backup ' +  time.asctime( time.localtime(time.time()) )
instance_id = 'i-3279e5ea'
device = '/dev/sda1'
region = 'us-west-2'

    
def backup():

    
    conn = ec2.connect_to_region(region)
    vols = conn.get_all_volumes(filters={'attachment.instance-id': instance_id})

    matches = [x for x in vols if x.attach_data.device == device]

    if (len(matches) == 1):
        jenkins_volume = matches[0]
    else:
        raise Exception('No attached volume could be found for %s' % device)

    print "found. the plan is to snapshot %s with tag %s" % (device, tag)

    snap = jenkins_volume.create_snapshot(snapshot_description(jenkins_volume, instance_id))
    snap.add_tag('Name', tag)

    print "done."

def snapshot_description(volume, instance_id):
    return "deployment snapshot of volume %s on instance %s" % (volume.id, instance_id)

if __name__ == '__main__':
    backup()
