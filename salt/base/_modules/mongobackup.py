from boto.s3.connection import S3Connection
import time
import subprocess
import os, math
from filechunkio import FileChunkIO
import re
from boto.s3.key import Key
import logging 
import uuid
import pymongo
import requests

log = logging.getLogger(__name__)

configCollections = ['available_offers','available_promotions','bot_buildings','bot_grids','bots','buckets','buildingstats','campaign_missions','cardgame','chest_spawns','cards','cards_requirements','chests','country_buffs','daily_missions','events','hardcoded_chests','misc','powerups','promotion_segments','store','server','unitstats','xplevels']

idrcollections = []

class RunCmd(object):
    def run_cmd(self, cmd):
        self.cmd = cmd
        subprocess.call(self.cmd, shell=True)

def _getMeHost():
    return __grains__['ip_interfaces']['eth0'][0]

def _filename():
    for f in os.listdir('/data'):
        if re.match('dumpfile', f):
            return f

def _dump():
    """
    function takes a dump of the entire mongo db instance 
    """
    host = _getMeHost()
    stop_balancer = 'mongo --host ' + host + ' /opt/stopbalancer.js'
    start_balancer = 'mongo --host ' + host + ' /opt/startbalancer.js'
    mongo_dump = 'mongodump --host ' + host + ' -o /data/backup'
    tar_cmd = 'tar -czf ' + '/data/dumpfile_' + str(uuid.uuid4()) + '.tar.gz'  + ' /data/backup'
    log.info("This is the tar command :" + tar_cmd )
    a = RunCmd()
    a.run_cmd("rm -rf /data/dumpfile*")
    a.run_cmd(stop_balancer)
    a.run_cmd(mongo_dump)
    a.run_cmd(start_balancer)
    a.run_cmd(tar_cmd)
    log.info("done with dump")

def _upload():
    conn = S3Connection('AKIAJWNBZPUTEB5HGTMQ','UdmB+zvP4qG1AAWV4IDb0F8a8peNptPPDH3OCrWb')
    if __grains__['game'] == 'rog':
        bucket = conn.get_bucket('rirogproductionbackup')
    elif __grains__['game'] == 'idr' and __grains__['randenv'] == 'test':
        bucket = conn.get_bucket('riidrstagingbackup')
    elif __grains__['game'] == 'idr' and __grains__['randenv'] == 'prod':
        bucket = conn.get_bucket('riidrprodbackup')

    k = Key(bucket)
    source_path = '/data/' + _filename()
    k.set_contents_from_filename(source_path)
    return k.key


def _getMeRestoreCmd():
    host = _getMeHost()
    if __grains__['randenv'] == 'prod':
        cmd = 'mongorestore --host ' + host + ' -d idr /opt/opt/dump/idrdev'
    else:
        cmd = 'mongorestore  /opt/opt/dump'
    return cmd

def operateOnUserColl(action):
    host = _getMeHost()
    global idrcollections
    client = pymongo.MongoClient(host, 27017)

    if __grains__['randenv'] == 'prod':
        db = client.idr
    else:
        db = client.idrdev

    idrcollections.extend( db.collection_names() )
    
    if action == 'drop':
        for c in idrcollections:
            if c not in configCollections:
                log.info('droping collection : ' + c)
                db.drop_collection(c)
    elif action == 'create':
        for c in config_collections:
            db.create_collection(c)

def _importUserData(key):
    """
    function imports  user data collections 
    """
    count = 0
    host = _getMeHost()
    stop_balancer = 'mongo --host ' + host + ' /opt/stopbalancer.js'
    start_balancer = 'mongo --host ' + host + ' /opt/startbalancer.js'
    a = RunCmd()
    a.run_cmd(stop_balancer)

    for c in idrcollections:
        if c not in configCollections:
            log.info('importing collection : ' + c)
            log.info('mongorestore -d idrdev -c ' + c + ' -h ' +  host + ' /data/data/backup/idr/' + c + '.bson')
            a.run_cmd('mongorestore -d idrdev -c ' + c + ' -h ' +  host + ' /data/data/backup/idr/' + c + '.bson')
    
    a.run_cmd(start_balancer)
    a.run_cmd("rm -rf /data/data")
    a.run_cmd("rm -rf /data/" + key)
    

def _fetchDump(key_value):
    host = _getMeHost()
    tar_cmd = 'tar -xvf ' + '/data/'+ key_value  + ' -C /data'
    a = RunCmd()
    conn = S3Connection('AKIAJWNBZPUTEB5HGTMQ','UdmB+zvP4qG1AAWV4IDb0F8a8peNptPPDH3OCrWb')
    bucket = conn.get_bucket('riidrprodbackup')
    key = bucket.get_key(key_value)
    key.get_contents_to_filename('/data/' + key_value)
    a.run_cmd(tar_cmd)

def restore(key):
    _fetchDump(key)
    operateOnUserColl('drop')
    _importUserData(key)
    return 'restored'

def filename():
    return _filename()
    

def backup():
    _dump()
    key= _upload()
    a = RunCmd()
    remove_file = 'rm -rf ' +  '/data/' + _filename()
    a.run_cmd(remove_file)
    r = requests.post('http://52.37.172.89:27179/hook/stagingdbRestore', data = {'key': key})
    log.info(r)
    return "Backup done S3 key ==> " + str(key)

if __name__ == '__main__':
    backup()
