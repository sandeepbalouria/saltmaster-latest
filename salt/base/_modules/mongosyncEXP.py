from boto.s3.connection import S3Connection
import time
import subprocess
import os, math
from filechunkio import FileChunkIO
import re
from boto.s3.key import Key
import logging 
import pymongo
import requests
from oss.oss_api import *
from oss import oss_xml_handler

log = logging.getLogger(__name__)

config_collections = ['available_offers','available_promotions','bot_buildings','bot_grids','bots','buckets','buildingstats','campaign_missions','cardgame','chest_spawns','cards','cards_requirements','chests','country_buffs','daily_missions','events','hardcoded_chests','misc','powerups','promotion_segments','store','server','unitstats','xplevels']

class RunCmd(object):
    def run_cmd(self, cmd):
        self.cmd = cmd
        subprocess.call(self.cmd, shell=True)

def _getMeHost():
    return __grains__['ip_interfaces']['eth0'][0]

def _filename():
    for f in os.listdir('/opt'):
        if re.match('configDump', f):
            return f

def _dumpCollections(colls):
    """
    function takes a dump of the collections contaiing 
    config data
    """
    host = _getMeHost()
    stop_balancer = 'mongo --host ' + host + ' /opt/stopbalancer.js'
    start_balancer = 'mongo --host ' + host + ' /opt/startbalancer.js'
    tar_cmd = 'tar -cvf ' + '/opt/configDump'  + '.tar.gz'  + ' /opt/dump'
    a = RunCmd()
    a.run_cmd("rm -rf /opt/configDump*")
    a.run_cmd("rm -rf /opt/opt")
    a.run_cmd("rm -rf /opt/dump")
    a.run_cmd(stop_balancer)
    for collection in colls:
        log.info(collection)
        a.run_cmd('mongodump -d idrdev -c ' + collection + ' -h ' +  host + ' -o /opt/dump')
    a.run_cmd(start_balancer)
    a.run_cmd(tar_cmd)

def _upload():
    conn = S3Connection('AKIAJWNBZPUTEB5HGTMQ','UdmB+zvP4qG1AAWV4IDb0F8a8peNptPPDH3OCrWb')
    if __grains__['game'] == 'idr' and __grains__['randenv'] == 'test':
        bucket = conn.get_bucket('devmongoconfigbackup')
    elif __grains__['game'] == 'idr' and __grains__['randenv'] == 'dev':
        bucket = conn.get_bucket('devmongoconfigbackup')
    log.info(bucket)
    k = Key(bucket)
    source_path = '/opt/' + _filename()
    log.info(source_path)
    k.set_contents_from_filename(source_path)
    return k.key


def _upload_china():
    endpoint="oss-cn-hangzhou.aliyuncs.com"
    accessKeyId, accessKeySecret="JUTDBgaemBexYYr4","Zr1xiEi7lx5Z9CL7k6tHBkdaMfvnKX"
    oss = OssAPI(endpoint, accessKeyId, accessKeySecret)

    source_path = '/opt/' + _filename()

    res=oss.put_object_from_file("devmongoconfig","devmongoconfig.tar.gz",source_path)
    log.info(res.status, res.read())

def _getMeRestoreCmd():
    host = _getMeHost()
    if __grains__['randenv'] == 'prod':
        cmd = 'mongorestore --host ' + host + ' -d idr /opt/opt/dump/idrdev'
    elif __grains__['randenv'] == 'test':
        cmd = 'mongorestore --host ' + host + ' -d idrdev /opt/opt/dump/idr'
    else:
        cmd = 'mongorestore --host ' + host + ' -d idrdev /opt/opt/dump/idr'
    return cmd

def operateOnConfigColl(action, colls):
    host = _getMeHost()
    client = pymongo.MongoClient(host, 27017)
    
    if __grains__['randenv'] == 'prod':
        db = client.idr
    else:
        db = client.idrdev

    if action == 'drop':
        for c in colls:
            log.info(c)
            db.drop_collection(c)
    elif action == 'create':
        for c in colls:
            db.create_collection(c)

def _import(key_value,colls):
    host = _getMeHost()
    stop_balancer = 'mongo --host ' + host + ' /opt/stopbalancer.js'
    start_balancer = 'mongo --host ' + host + ' /opt/startbalancer.js'
    tar_cmd = 'tar -xvf ' + '/opt/'+ key_value + '.tar.gz'  + ' -C /opt'
    restore_cmd = _getMeRestoreCmd()
    a = RunCmd()
    a.run_cmd("rm -rf /opt/*.tar.gz")
    a.run_cmd("rm -rf /opt/opt")
    a.run_cmd(stop_balancer)
    conn = S3Connection('AKIAJWNBZPUTEB5HGTMQ','UdmB+zvP4qG1AAWV4IDb0F8a8peNptPPDH3OCrWb')
    bucket = conn.get_bucket('devmongoconfigbackup')
    key = bucket.get_key(key_value)
    key.get_contents_to_filename('/opt/' + key_value + '.tar.gz')
    a.run_cmd(tar_cmd)
    operateOnConfigColl('drop',colls)
    a.run_cmd(restore_cmd)
    a.run_cmd(start_balancer)


def exportConfig(collections='ALL',location='aws',user='NoUser'):
    """
    """
    if collections == 'ALL':
        collections_dic = config_collections
    else:
        collections_dic = collections.split(".")
    if __grains__['role'] == 'mongo-dev' or __grains__['role'] == 'push-server':
        if location == 'aws':
            _dumpCollections(collections_dic)
            key = _upload()
            return "config exported successfully : " + str(key) 
        elif location == 'china':
            _dumpCollections(collections_dic)
            _upload_china()
            return "file uploaded successfully to alicloud bucket"
    else:
        return 'Not a mongo node'
def importConfig(key_value,collections='ALL'):
    """
    """
    if collections == 'ALL':
        collections_dic = config_collections
    else:
        collections_dic = collections.split(".")
    _import(key_value,collections_dic)
    return "successfully imported"
    
if __name__ == '__main__':
    print 'hi'
