import git
import subprocess

idr_cmd = 'supervisorctl restart idr'
rog_cmd = 'supervisorctl restart rog'
return_string = ''

class RunCmd(object):
    def run_cmd(self, cmd):
        self.cmd = cmd
        return subprocess.call(self.cmd, shell=True)


def update():
    
    if __grains__['game'] == 'idr' and __grains__['role'] == 'node-testing' or __grains__['role'] == 'app-dev':
        git_dir = '/home/idr/IDR-Server'
        g = git.cmd.Git(git_dir)
        g.pull()
        subprocess.call('chown -R idr:idr /home/idr/IDR-Server', shell=True)
        if subprocess.call(idr_cmd, shell=True) == 0:
            return_string = g.log('-1','--pretty=oneline')
        else:
            return 'update failed'
    
    if __grains__['erating'] == 'enabled':
        git_dir = '/home/erating/Erating-Forward'
        g = git.cmd.Git(git_dir)
        g.pull()
        subprocess.call('chown -R erating:erating /home/erating/Erating-Forward', shell=True)
        if subprocess.call(idr_cmd, shell=True) == 0:
            return_string +=  '\n' + g.log('-1','--pretty=oneline')
        else:
            return 'update failed'
    
    if __grains__['game'] == 'rog' and __grains__['role'] == 'node-testing':
        if __grains__['randenv'] == 'test':
            update_cmd = "su - rog -c 'cd /home/rog/rog_ri_staging/ && svn update'"
        elif __grains__['randenv'] == 'prod':
            update_cmd = "su - rog -c 'cd /home/rog/rog_ri_production/ && svn update'"
        a = RunCmd()
        if a.run_cmd(update_cmd) == 0:
            if a.run_cmd(rog_cmd) == 0:
                return 'successfully updated svn repo'
            else:
                return 'failed in restarting supervisor'
        else:
            return 'failed in update command '
    return return_string
