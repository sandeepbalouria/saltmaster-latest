import git
import subprocess

docker_restart = 'service docker restart'
docker_build = "cd /root/rb-server-docker && docker build -t 'pvpserver:dockerfile' ."
docker_run = "/bin/bash /root/pvpserver.sh"

def update():
    
    if __grains__['role'] == 'pvpserver':
        git_dir = '/root/rb-server-docker'
        g = git.cmd.Git(git_dir)
        g.pull()
        subprocess.call(docker_restart, shell=True)
        subprocess.call(docker_build, shell=True)
        if subprocess.call(docker_run, shell=True) == 0:
            return_string = g.log('-1','--pretty=oneline')
        else:
            return 'update failed'
        return return_string
    else:
        return "Not a pvpserver node"
    
