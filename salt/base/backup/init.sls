{% if grains['role'] == 'push-server' or grains['role'] == 'mongo-dev'  or grains['role'] == 'node-testing'%}
/opt/stopbalancer.js:
  file.managed:
    - source: salt://backup/files/stopbalancer.js

/opt/startbalancer.js:
  file.managed:
    - source: salt://backup/files/startbalancer.js

{% endif %}
{% if grains['role'] == 'push-server' and grains['randenv'] == 'prod' %}
backup-schedule:
  schedule.present:
    - function: mongobackup.backup
    - hours: 24 
{% endif %}


