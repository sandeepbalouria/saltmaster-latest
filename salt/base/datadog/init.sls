datadog:
  pkgrepo.managed:
    - name: deb http://apt.datadoghq.com/ stable main
    - file: /etc/apt/sources.list.d/datadog.list
    - keyserver: keyserver.ubuntu.com
    - keyid: C7A7DA52

  pkg.installed:
    - name: datadog-agent
    - require:
      - pkgrepo: datadog

  service.running:
    - enable: True
    - name: datadog-agent
    - require:
      - pkg: datadog
    - watch:
      - file: /etc/dd-agent/datadog.conf

/etc/dd-agent/datadog.conf:
  file.managed:
    - template: jinja
    - source: salt://datadog/files/datadog.conf
