{% if  grains['firstrun'] == True %}

reset_firstrun:
  cmd.run:
    - name: salt-call  grains.setval firstrun False

firstrun_reboot:
    cmd.run:
      - name: reboot

{% endif %}

{% if grains['firstrun'] == False %}

myarecord:
  boto_route53.present:
    - name: {{ grains['hostname'] }}.
    - value: {{ grains['ip_interfaces']['eth0'][0] }}
    - zone: usoregon.local.
    - ttl: 60
    - record_type: A
    - region: us-west-2
    - profile:
        keyid: AKIAJMANVJBUP7IIFK3Q
        key: nLcvAdTSsKY7+xisKsrVm9hSlC7+T1R0MUOjgUhV

reset_firstrun:
  cmd.run:
    - name: salt-call  grains.setval firstrun NotAnyMore

{% endif %}
