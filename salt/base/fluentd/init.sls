include:
  - fluentd.packages

td-agent-conf:
  file.managed:
    - name: /etc/td-agent/td-agent.conf
    - template: jinja
    - source: salt://fluentd/files/td-agent.conf.jinja
    - mode: 644
    - user: root
    - group: root

fluent-pos-dir:
  file.directory:
    - name: /var/log/fluent
    - mode: 755
    - user: td-agent
    - group: td-agent
    - makedirs: True
permission-tm-log:
  cmd.run:
    - name: chmod 777 -R /tm/logs
permission-nginx-log:
  cmd.run:
    - name: chmod 777 -R /var/log/nginx
td-agent:
  service.running:
    - enable: True

