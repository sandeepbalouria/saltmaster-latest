packages:
  pkg.installed:
    - pkgs:
      - ruby
      - td-agent

fluent-plugin-kinesis-alt:
  cmd.run:
    - name: td-agent-gem install fluent-plugin-logzio
    - unless: td-agent-gem list --local | grep fluent-plugin-logzio
  
fluent-plugin-record-reformer:
  cmd.run:
    - name: td-agent-gem install fluent-plugin-record-reformer
    - unless: td-agent-gem list --local | grep fluent-plugin-record-reformer



