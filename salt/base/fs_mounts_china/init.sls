lvm2:
  pkg.installed

{% if grains['role'] == 'mongo-shard' or grains['role'] == 'mongo-dev' and grains['firstrun'] == True %}

/dev/vdb:
  lvm.pv_present:
    - require:
      - pkg: lvm2

vg0:
  lvm.vg_present:
    - devices: /dev/vdb

vg0-lv_mongodb:
  lvm.lv_present:
    - vgname: vg0
    - size: 180G
    - stripes: 1
    - stripesize: 8K


/dev/vg0/vg0-lv_mongodb:
  cmd.wait:
    - name: mkfs.ext4 /dev/vg0/vg0-lv_mongodb
    - require:
      - lvm: vg0-lv_mongodb
    - watch:
      - lvm:  vg0-lv_mongodb

/mongodb:
  mount.mounted:
    - device: /dev/vg0/vg0-lv_mongodb
    - fstype: ext4
    - mkmnt: True
    - opts:
      - defaults
    - user: root
    - require:
      - cmd: /dev/vg0/vg0-lv_mongodb

{% endif %}

