user-proferi:
  user.present:
    - name: proferi
    - fullname: proferi
    - shell: /bin/bash
    - createhome: false
    - home: /opt/proferi
    #{% if grains['role'] != 'tp-tuplecache'  %}
swap-fs:
  cmd.run:
    - name: mkswap -f /dev/xvdb
    - unless: swapon -s | grep -q "partition"
    
swap-on:
  cmd.run:
    - name: swapon /dev/xvdb
    - unless: swapon -s  | grep -q "partition"
    - require:
      - cmd: swap-fs

swap-cmd:
  cmd.run:
    - name: echo '/dev/xvdb   swap   swap   defaults  0 0' >> /etc/fstab 
    - unless: cat /etc/fstab | grep -q "swap"
    - require:
      - cmd: swap-on
      
      #{% endif %}
{% if grains['clusterType'] == 'SingleNodeExp'  %}

#/dev/xvdf:
  #  blockdev.formatted:
    #  - require:
      #  - user: user-proferi


/dev/xvdf:
  lvm.pv_present

vg0:
  lvm.vg_present:
    - devices: /dev/xvdf

vg0-lv_opt:
  lvm.lv_present:
    - vgname: vg0
    - size: 10G
    - stripes: 1
    - stripesize: 8K

vg0-lv_tm:
  lvm.lv_present:
    - vgname: vg0
    - size: 10G
    - stripes: 1
    - stripesize: 8K

/dev/vg0/vg0-lv_tm:
  blockdev.formatted:
    - require:
      - user: user-proferi
      - lvm: vg0-lv_tm

/dev/vg0/vg0-lv_opt:
  blockdev.formatted:
    - require:
      - user: user-proferi
      - lvm: vg0-lv_opt


/opt:
  mount.mounted:
    - device: /dev/vg0/vg0-lv_opt
    - fstype: ext4
    - mkmnt: True
    - opts:
      - defaults
    - user: root
    - require:
      - user: user-proferi
      - blockdev: /dev/vg0/vg0-lv_opt

/tm:
  mount.mounted:
    - device: /dev/vg0/vg0-lv_tm
    - fstype: ext4
    - mkmnt: True
    - opts:
      - defaults
    - user: root
    - require:
      - user: user-proferi
      - blockdev: /dev/vg0/vg0-lv_tm

tm_permissions:
  file.directory:
    - name: /tm
    - user: proferi
    - group: proferi
    - require:
      - mount: /tm

{% endif %}

{% if grains['role'] == 'web'  %}

/dev/xvdf:
  blockdev.formatted:
    - require:
      - user: user-proferi

/dev/xvdh:
  blockdev.formatted:
    - require:
      - user: user-proferi

/tm:
  mount.mounted:
    - device: /dev/xvdf
    - fstype: ext4
    - mkmnt: True
    - opts:
      - defaults
    - user: root
    - require:
      - user: user-proferi

/opt:
  mount.mounted:
    - device: /dev/xvdh
    - fstype: ext4
    - mkmnt: True
    - opts:
      - defaults
    - user: root

tm_permissions:
  file.directory:
    - name: /tm
    - user: proferi
    - group: proferi
    - require:
      - mount: /tm


{% endif %}
{% if grains['role'] == 'data' or grains['role'] == 'tp-data'  %}

/dev/xvdg:
  blockdev.formatted:
    - require:
      - user: user-proferi

/pgdata:
  mount.mounted:
    - device: /dev/xvdg
    - fstype: ext4
    - mkmnt: True
    - opts:
      - defaults
    - user: root

{% endif %}
{% if grains['role'] == 'tp-hdfs' or grains['role'] == 'tp-servicenode' or grains['role'] == 'tp-tuplecache'%}

/dev/xvdf:
  blockdev.formatted:
    - require:
      - user: user-proferi

/dev/xvdh:
  blockdev.formatted:
    - require:
      - user: user-proferi

/tm:
  mount.mounted:
    - device: /dev/xvdf
    - fstype: ext4
    - mkmnt: True
    - opts:
      - defaults
    - user: root
    - require:
      - user: user-proferi

/opt:
  mount.mounted:
    - device: /dev/xvdh
    - fstype: ext4
    - mkmnt: True
    - opts:
      - defaults
    - user: root

tm_permissions:
  file.directory:
    - name: /tm
    - user: proferi
    - group: proferi
    - require:
      - mount: /tm

{% endif %}
