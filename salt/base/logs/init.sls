filemonitoring-conf:
  file.managed:
    - name: /etc/rsyslog.d/21-filemonitoring.conf
    - source: salt://logs/files/filemonitoring.conf
    - template: jinja
    - mode: 644
    - user: root
    - group: root
22-loggly.conf:
  file.managed:
    - name: /etc/rsyslog.d/22-loggly.conf
    - source: salt://logs/files/22-loggly.conf
    - template: jinja
    - mode: 644
    - user: root
    - group: root
{% if grains['game'] == 'idr' %}
logs-dir:
  file.directory:
    - name: /home/idr/logs/
    - user: idr
    - group: idr
    - mode: 755
    - makedirs: True
{% endif %}
{% if grains['game'] == 'rog' %}
rog:
  user.present:
    - home: /home/rog
logs-dir:
  file.directory:
    - name: /home/rog/logs/
    - user: rog
    - group: rog
    - mode: 755
    - makedirs: True
    - require:
      - user: rog
        

{% endif %}

rsyslog-service:
  service:
    - name: rsyslog
    - running
    - watch:
      - file: /etc/rsyslog.d/21-filemonitoring.conf
