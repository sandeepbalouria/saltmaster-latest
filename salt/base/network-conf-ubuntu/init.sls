network-ifcg-eth0:
  file.managed:
    - name: /etc/network/interfaces.d/eth0.cfg
    - source: salt://network-conf-ubuntu/files/ifcfg-eth0
    - template: jinja
    - user: root
    - group: root
    - mode: 600
network-hostname:
  file.managed:
    - name: /etc/hostname
    - source: salt://network-conf-ubuntu/files/hostname
    - template: jinja
    - user: root
    - group: root
    - mode: 600

edit-cloud.cfg:
  cmd.wait:
    - name: "echo 'preserve_hostname: True' >> /etc/cloud/cloud.cfg"
    - watch:
      - file: network-ifcg-eth0


