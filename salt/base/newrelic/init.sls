newrelic-repo:
  pkg.installed:
    - sources:
      - newrelic-repo: http://download.newrelic.com/pub/newrelic/el5/x86_64/newrelic-repo-5-3.noarch.rpm

newrelic-sysmond:
  pkg.installed

newrelic-conf:
  file.managed:
    - name: /etc/newrelic/nrsysmond.cfg
    - source: salt://newrelic/files/nrsysmond.cfg
    - mode: 640

newrelic-sysmond-service:
  service.running:
    - name: newrelic-sysmond
    - require:
      - file: newrelic-conf
