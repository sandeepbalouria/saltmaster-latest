{% set nginx        = pillar.get('nginx', {}) -%}

nginx-package:
  pkg.installed:
    - pkgs:
      - nginx
      - libpng
      - fontconfig

nginx-conf:
  file.managed:
    - name: /etc/nginx/nginx.conf
    - template: jinja
    - source: salt://nginx/files/nginx.conf
    - mode: 644
    - user: root
    - group: root
    - require:
      - pkg: nginx-package

nginx-conf-dir:
  file.directory:
    - name: /etc/nginx/conf.d
    - user: root
    - group: root
    - makedirs: True
    
{% if grains['clusterType'] == 'SingleNode' %}
nginx-presentation.conf:
  file.managed:
    - name: /etc/nginx/conf.d/presentation.conf
    - template: jinja
    - source: salt://nginx/templates/presentation.conf.tp.jinja
    - mode: 644
    - user: root
    - group: root
    - require:
      - pkg: nginx-package
      - file: nginx-conf-dir
{% endif %}

{% if grains['clusterType'] == 'MultiNode' %}
nginx-presentation.conf:
  file.managed:
    - name: /etc/nginx/conf.d/presentation.conf
    - template: jinja
    - source: salt://nginx/templates/presentation.conf.jinja
    - mode: 644
    - user: root
    - group: root
    - require:
      - pkg: nginx-package
      - file: nginx-conf-dir
{% endif %}

{% if grains['clusterType'] == 'TenantPartition' %}
nginx-presentation.conf:
  file.managed:
    - name: /etc/nginx/conf.d/presentation.conf
    - template: jinja
    - source: salt://nginx/templates/presentation.conf.tp.jinja
    - mode: 644
    - user: root
    - group: root
    - require:
      - pkg: nginx-package
      - file: nginx-conf-dir
{% endif %}

nginx-mime-types:
  file.managed:
    - name: /etc/nginx/mime.types
    - source: salt://nginx/files/mime.types
    - mode: 644
    - user: root
    - group: root
    - require:
      - pkg: nginx-package
       
nginx-service:
  service:
    - name: nginx
    - running
    - require:
      - pkg: nginx-package
    - watch:
      - file: /etc/nginx/nginx.conf
      - file: /etc/nginx/conf.d/presentation.conf
      - pkg: nginx-package

nginx-keys-dir:
  file.directory:
    - name: /etc/nginx/keys
    - user: root
    - group: root
    - mode: 755
    - require:
      - pkg: nginx-package

nginx-tidemark-cert:
  file.managed:
    - name: /etc/nginx/keys/wildcard.tidemark.net.cert
    - source: salt://nginx/files/wildcard.tidemark.net.cert
    - mode: 644
    - user: root
    - group: root
    - require:
      - file: nginx-keys-dir

nginx-tidemark-private:
  file.managed:
    - name: /etc/nginx/keys/wildcard.tidemark.net.private.key
    - source: salt://nginx/files/wildcard.tidemark.net.private.key
    - mode: 644
    - user: root
    - group: root
    - require:
      - file: nginx-keys-dir

nginx-service-enable:
  cmd.run:
    - name: systemctl enable nginx.service
