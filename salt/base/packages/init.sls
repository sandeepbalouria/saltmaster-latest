random_packages:
  pkg.installed:
    - pkgs:
      - unzip
      - python-pip
      - gcc
      - python-dev
      - htop
      - supervisor
      - iotop
      - git
      - ntp
    
boto:
  pip.installed
psutil:
  pip.installed
gitpython:
  pip.installed

{% if grains['role'] == 'app-dev' %}
packages:
  pkg.installed:
    - pkgs:
      - redis-server
{% endif %}
{% if grains['role'] == 'push-server' or grains['role'] == 'mongo-dev' %}
filechunkio:
  pip.installed
pymongo:
  pip.installed
aliyun-python-sdk-oss:
  pip.installed
random_pack:
  pkg.installed:
    - pkgs:
      - mongodb-org-shell
      - mongodb-org-tools
{% endif %}



