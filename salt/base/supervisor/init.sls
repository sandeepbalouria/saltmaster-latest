supervisor:
  service.running:
    - enable: True
{% if grains['role'] == 'app-dev' or grains['role'] == 'node-server' or  grains['role'] == 'node-testing' %}
{% if grains['game'] == 'rog' %}
/etc/supervisor/conf.d/node.conf:
  file.managed:
    - template: jinja
    - source: salt://supervisor/files/node-rog.conf.jinja
{% endif %}
{% if grains['game'] == 'idr' %}
/etc/supervisor/conf.d/node.conf:
  file.managed:
    - template: jinja
    - source: salt://supervisor/files/node.conf.jinja
{% endif %}
{% endif %}
{% if grains['role'] == 'cstool' %}
/etc/supervisor/conf.d/node.conf:
  file.managed:
    - template: jinja
    - source: salt://supervisor/files/node-cstool.conf.jinja
{% endif %}
{% if grains['role'] == 'push-server' %}
/etc/supervisor/conf.d/pushserver.conf:
  file.managed:
    - template: jinja
    - source: salt://supervisor/files/pushserver.conf.jinja
{% endif %}
{% if grains['erating'] == 'enabled' %}
/etc/supervisor/conf.d/erating.conf:
  file.managed:
    - template: jinja
    - source: salt://supervisor/files/erating.conf.jinja
{% endif %}


