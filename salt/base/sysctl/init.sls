rhel_sysctl_conf:
  file.managed:
    - name: /etc/sysctl.conf
    - source: salt://sysctl/files/sysctl.conf
    - user: root
    - group: root
    - mode: 644
  cmd.wait:
    - name: /sbin/sysctl -p
    - watch:
      - file: rhel_sysctl_conf

rhel_limits_conf:
  file.managed:
    - name: /etc/security/limits.d/20-nproc.conf
    - source: salt://sysctl/files/20-nproc.conf
    - user: root
    - group: root
    - mode: 644
