timezone-set:
  cmd.run:
    - name: timedatectl set-timezone UTC
    - unless: date | grep -q 'UTC'


