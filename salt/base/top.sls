base:
    'role:mongo-config':
        - match: grain
        - network-conf-ubuntu
        - packages
        - firstrun
        - users
        - mongodb.mongodbconfig
    'role:node-server':
        - match: grain
        - network-conf-ubuntu
        - packages
        - firstrun
        - users
        - nodejs
        - mongodb.mongos
        - logs
        - datadog
        - supervisor
    'role:mongo-shard':
        - match: grain
        - fs_mounts
        - network-conf-ubuntu
        - packages
        - firstrun
        - users
        - mongodb
    'role:app-dev':
        - match: grain
        - network-conf-ubuntu
        - packages
        - firstrun
        - users
        - nodejs
        - logs
        - supervisor
    'role:mongo-dev':
        - match: grain
        - fs_mounts
        - network-conf-ubuntu
        - packages
        - firstrun
        - users
        - mongodb
        - backup
    'role:cstool':
        - match: grain
        - network-conf-ubuntu
        - packages
        - firstrun1
        - users
        - nodejs
        - supervisor
    'role:node-testing':
        - match: grain
        - packages
        - users
        - nodejs
        - mongodb.mongos
        - logs
        - supervisor
        - backup
    'role:push-server':
        - match: grain
        - network-conf-ubuntu
        - packages
        - firstrun
        - users
        - nodejs
        - mongodb.mongos
        - logs
        - supervisor
        - backup
