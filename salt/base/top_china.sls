base:
    'role:mongo-config':
        - match: grain
        - packages
        - users
        - mongodb.mongodbconfig
    'role:mongo-shard':
        - match: grain
        - fs_mounts_china
        - packages
        - users
        - mongodb
    'role:cstool':
        - match: grain
        - packages
        - users
        - nodejs
        - supervisor
    'role:node-testing':
        - match: grain
        - packages
        - users
        - nodejs
        - mongodb.mongos
        - supervisor
    'role:push-server':
        - match: grain
        - packages
        - users
        - nodejs
        - mongodb.mongos
        - supervisor
    'role:mongo-dev':
        - match: grain
        - packages
        - users
        - mongodb
