base:
    'role:mongo-config':
        - match: grain
        - network-conf-ubuntu
        - packages
        - firstrun1
        - users
        - mongodb.mongodbconfig
    'role:node-server':
        - match: grain
        - network-conf-ubuntu
        - packages
        - firstrun1
        - users
        - nodejs
        - mongodb.mongos
        - logs
        - datadog
        - supervisor
    'role:mongo-shard':
        - match: grain
        - fs_mounts
        - network-conf-ubuntu
        - packages
        - firstrun1
        - users
        - mongodb
        - datadog
    'role:app-dev':
        - match: grain
        - network-conf-ubuntu
        - packages
        - firstrun1
        - users
        - nodejs
        - logs
        - datadog
        - supervisor
    'role:mongo-dev':
        - match: grain
        - fs_mounts
        - network-conf-ubuntu
        - packages
        - firstrun1
        - users
        - mongodb
    'role:cstool':
        - match: grain
        - network-conf-ubuntu
        - packages
        - firstrun1
        - users
        - nodejs
        - supervisor
    'role:node-testing':
        - match: grain
        - packages
        - users
        - nodejs
        - mongodb.mongos
        - logs
        - datadog
        - supervisor
    'role:push-server':
        - match: grain
        - network-conf-ubuntu
        - packages
        - firstrun1
        - users
        - nodejs
        - mongodb.mongos
        - logs
        - supervisor
        - backup
