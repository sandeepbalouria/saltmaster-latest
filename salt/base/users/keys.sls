ssb:
  ssh_auth.present:
    - user: ssb
    - source: salt://users/files/sbalouria.id_rsa.pub
sandeep:
  ssh_auth.present:
    - user: sandeepbalouria
    - source: salt://users/files/sandeepbalouria.id_rsa.pub
{% if grains['erating'] == 'enabled' %}
aidan:
  ssh_auth.present:
    - user: aidan
    - source: salt://users/files/aidan.id_rsa.pub
{% endif %}

