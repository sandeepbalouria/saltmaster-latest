sudoers-file:
  file.managed:
    - name: /etc/sudoers
    - source: salt://users/files/sudoers
