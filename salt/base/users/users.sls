sandeep-user:
  user.present:
    - name: ssb 
    - fullname: sandeep balouria
    - shell: /bin/bash
    - home: /home/ssb
sandeepbalouria-user:
  user.present:
    - name: sandeepbalouria
    - fullname: sandeep balouria
    - shell: /bin/bash
    - home: /home/sandeepbalouria
{% if grains['game'] == 'idr' %}
idr-user:
  user.present:
    - name: idr 
    - fullname: idr
    - shell: /bin/bash
    - home: /home/idr
{% endif %}
{% if grains['erating'] == 'enabled' %}
erating-user:
  user.present:
    - name: erating
    - fullname: erating
    - shell: /bin/bash
    - home: /home/erating
aidan-user:
  user.present:
    - name: aidan
    - fullname: aidan
    - shell: /bin/bash
    - home: /home/aidan
{% endif %}
{% if grains['game'] == 'rog' %}
rog-user:
  user.present:
    - name: rog
    - fullname: rog
    - shell: /bin/bash
    - home: /home/rog
{% endif %}
{% if grains['location'] == 'china' %}
ubuntu-user:
  user.present:
    - name: ubuntu
    - fullname: ubuntu
    - shell: /bin/bash
    - home: /home/ubuntu
{% endif %}

